export default function Error() {
    return (
        <>
        <h1><strong>Page not Found</strong></h1>
        <h4>Go back to the <a href="/">homepage.</a></h4>
        </>
    )
}
